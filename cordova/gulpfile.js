var
  gulp = require('gulp'),
  jade = require('gulp-jade'),
  plumber = require('gulp-plumber'),
  browserSync = require('browser-sync').create(),
  sass = require('gulp-sass'),
  ts = require('gulp-typescript'),
  context = {
    mode: 'mobile'
  };

var paths = {
  sass: {
    watch: './sass/**/*.sass',
    compile: './www/css'
  },
  jade: {
    watch: './jade/**/*.jade',
    compile: './www',
  },
  js: {
    watch: './src/**/*.ts',
    compiled: './www/**/*.js',
  },
};

gulp.task('sass', function () {
  return gulp.src(paths.sass.watch)
    .pipe(plumber({
        handleError: function (err) {
            console.log(err);
            this.emit('end');
        }
    }))
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest(paths.sass.compile))
    .pipe(browserSync.reload({stream: true}));
});

gulp.task('jade', function() {
  return gulp.src(paths.jade.watch)
    .pipe(plumber({
        handleError: function (err) {
            console.log(err);
            this.emit('end');
        }
    }))
    .pipe(jade({
      locals: {
        context: context
      }
    }))
    .pipe(gulp.dest(paths.jade.compile))
    .pipe(browserSync.reload({stream: true}));
});

gulp.task('dev-watch', function () {
  gulp.watch(paths.sass.watch, ['sass']);
  gulp.watch(paths.jade.watch, ['init-browser' ,'jade']);
});

var del = require('del');

gulp.task('clean-www', function () {
  return del([
    'www/index.html',
    'www/templates/*',
    'www/css/*',
    'www/js/*'
  ]);
});

gulp.task('compile-ts', function() {
  var tsProject = ts.createProject('./src/tsconfig.json')
  tsResult = tsProject.src() // instead of gulp.src(...)
		.pipe(ts(tsProject));

	return tsResult.js.pipe(gulp.dest('./www/js'));
});

gulp.task('init-browser', function () {
  context.mode = 'browser';
});

gulp.task('init-mobile', function () {
  context.mode = 'mobile';
});

gulp.task('serve-browser', function () {
  context.mode = 'browser';
  browserSync.init({
    server: {
      baseDir: './www/'
    }
  });
  gulp.watch(paths.sass.watch, ['sass']);
  gulp.watch(paths.jade.watch, ['jade']);

  browserSync.watch(paths.js.compiled).on('change', browserSync.reload);
});

gulp.task('browser-sync', function () {
  context.mode = 'browser';
  browserSync.init({
    server: {
      baseDir: './www/'
    }
  });
});

gulp.task('build-browser', ['init-browser', 'clean-www', 'sass', 'jade', 'compile-ts']);

gulp.task('build-mobile', ['init-mobile', 'clean-www', 'sass', 'jade', 'compile-ts']);

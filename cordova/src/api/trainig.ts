import {Point} from './point';

export enum TrainingStatus {
    NotDefied,
    Working,
    Stoped
}

export class Training {
  public startTime:Date = null;
  public finishTime:Date = null;
  public status:TrainingStatus = TrainingStatus.NotDefied;
  public points: Point[] = [];

  public constructor() {
  }

  public static fromDict(dict:any):Training {
    let training:Training = new Training();
    let startTime:Date = dict.startTicks == 'null' ? null : new Date(dict.startTicks);
    let finishTime:Date = dict.finishTicks == 'null' ? null : new Date(dict.finishTicks);
    training.startTime = startTime;
    training.finishTime = finishTime;
    training.status = dict.status;
    for (let pointDict of dict.points) {
      training.points.push(Point.fromDict(pointDict));
    }

    return training;
  }

  public toJSON() {
    let startTicks = this.startTime != null ? this.startTime.valueOf() : 'null';
    let finishTicks = this.finishTime != null ? this.finishTime.valueOf() : 'null';
    return {
      startTicks: startTicks,
      finishTicks: finishTicks,
      status: this.status,
      points: this.points
    }
  }

  public start() {
    if (this.status == TrainingStatus.Stoped) {
      throw new Error('The training could not be started.');
    }
    this.startTime = new Date();
    this.status = TrainingStatus.Working;
  }

  public stop() {
    if (this.status != TrainingStatus.Working) {
      throw new Error('The not working training could not be stoped.');
    }
    this.finishTime = new Date();
    this.status = TrainingStatus.Stoped;
  }

  public getDuration(): number {
    let endTime = this.finishTime || new Date();
    var diff:number = endTime.getTime() - this.startTime.getTime();
    return diff;
  }
}

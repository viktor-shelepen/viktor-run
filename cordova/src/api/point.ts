///<reference path="../../typings/googlemaps/google.maps.d.ts"/>

export class Point {
    public constructor(public latLng:google.maps.LatLng, public timestamp:number) {
    }

    public static fromDict(dict:any):Point {
      let point:Point = new Point(
        new google.maps.LatLng(
          dict.lat,
          dict.lng
        ),
        dict.timestamp
      );

      return point;
    }

    public toJSON() {
      return {
        latLng: this.latLng,
        timestamp: this.timestamp
      }
    }
}

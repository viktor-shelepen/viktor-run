import {Component} from 'angular2/core';
import {ConfigService} from '../config.service';
import {Video} from '../video';
import {MapPageComponent} from './page/map';
import {ROUTER_DIRECTIVES, ROUTER_PROVIDERS, RouteConfig, Router, RouteParams} from 'angular2/router';
import {PlayerPageComponent} from './page/player';
import {HistoryPageComponent} from './page/history';

declare let window:Window;

@Component({
    selector: 'my-app',
    templateUrl: './templates/component/app.html',
    directives: [ROUTER_DIRECTIVES],
    providers: []
})
@RouteConfig([
  {
    path:'/player',
    name: 'PlayerPage',
    component: PlayerPageComponent,
    useAsDefault: true
  },
  {path:'/map', name: 'MapPage', component: MapPageComponent},
  {path:'/history', name: 'HistoryPage', component: HistoryPageComponent},
])
export class AppComponent {
  constructor() {
  }

  public reloadPage() {
    window.location.reload();
  }

  public clearCache() {
    window.localStorage.clear();
    window.location.reload();
  }

}

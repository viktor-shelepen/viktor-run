///<reference path="../../../typings/googlemaps/google.maps.d.ts"/>

import {Component, OnInit, OnDestroy, ElementRef, Output} from 'angular2/core';
import {PlayerService} from '../../service/player';
import {Point} from '../../api/point';
declare var google: any;
declare var navigator: any;

@Component({
    selector: 'map-page',
    templateUrl: './templates/component/page/map.html',
    styles: [`
      .map {
        height: 80vh;
        width: 100%;
      }
    `]
})
export class MapPageComponent implements OnInit, OnDestroy {
    public map: google.maps.Map = null;
    public marker: google.maps.Marker = null;
    public route: google.maps.Polyline = null;
    currentPoint: Point = null;

    constructor(private _elem: ElementRef, private playerService: PlayerService) {
        this.playerService.positionUpdatedEvent.subscribe((point: Point) => {
            this.currentPoint = point;
            this.map.setCenter(point.latLng);
            this.marker.setPosition(point.latLng);
        });

        this.playerService.routeUpdatedEvent.subscribe((data:any) => {
            let path: google.maps.LatLng[] = this.getPath();
            this.route.setPath(path);
        });
    }

    public getPath(): google.maps.LatLng[] {
        if (this.playerService.training == null) {
            return [];
        }
        let path: google.maps.LatLng[] = [];
        for (let point of this.playerService.training.points) {
            path.push(point.latLng);
        }

        return path;
    }

    ngOnInit() {
        const container = this._elem.nativeElement.querySelector('.map');
        this.map = new google.maps.Map(container, {
            center: { lat: -34.397, lng: 150.644 },
            zoom: 13
        });
        this.route = new google.maps.Polyline({
            geodesic: true,
            strokeColor: '#FF0000',
            strokeOpacity: 1.0,
            strokeWeight: 2
        });
        this.route.setMap(this.map);
        this.marker = new google.maps.Marker({
            icon: {
                path: google.maps.SymbolPath.BACKWARD_CLOSED_ARROW,
                scale: 2
            },
            draggable: true,
            map: this.map
        });
    }

    ngOnDestroy() {
    }
}

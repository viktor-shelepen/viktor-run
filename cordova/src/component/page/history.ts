import {Component} from 'angular2/core';
import {PlayerService} from '../../service/player';
import {Training} from '../../api/trainig';

@Component({
    selector: 'history-page',
    templateUrl: './templates/component/page/history.html',
})
export class HistoryPageComponent {
  public trainings:Training[];
  constructor(public playerService: PlayerService) {
    this.trainings = playerService.trainings;
  }
}

import {Component} from 'angular2/core';
import {PlayerService} from '../../service/player';
import {TrainingStatus} from '../../api/trainig';

@Component({
    selector: 'player-page',
    templateUrl: './templates/component/page/player.html',
    providers: []
})
export class PlayerPageComponent {
    public toggleLabel:string = 'Start';

    constructor(private playerService: PlayerService) {
      if (
        this.playerService.training != null
        && this.playerService.training.status == TrainingStatus.Working
      ) {
        this.toggleLabel = 'Stop';
      }
    }

    public toggleTraining() {
      if (this.playerService.training == null) {
        this.playerService.startTraining();
        this.toggleLabel = 'Stop';
      }
      else {
        this.playerService.stopTraining();
        this.toggleLabel = 'Start';
      }
    }

}

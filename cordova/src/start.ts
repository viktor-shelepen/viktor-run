/// <reference path="./typings/systemjs/systemjs.d.ts"/>
declare var baseUrl;

function runApp() {
  System.config({
    baseUrl: baseUrl,
    packages: {
      'js': {
        format: 'register',
        defaultExtension: 'js'
      }
    }
  });
  System.import(baseUrl + 'js/main.js')
    .then(null, console.error.bind(console));
};

function deviceReady() {
  var element:HTMLElement = document.getElementById('startScreen');
  var parentEl:Node = element.parentNode;
  parentEl.removeChild(element);
  runApp();
};

document.addEventListener(
  "deviceready",
  () => {
    deviceReady();
  },
  false
);

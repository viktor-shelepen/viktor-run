///<reference path="../../typings/googlemaps/google.maps.d.ts"/>
import { Injectable, EventEmitter } from 'angular2/core';
import {Training, TrainingStatus} from '../api/trainig';
import {Point} from '../api/point';

declare let window:Window;
declare let InjectionPlugin:any;

interface ServiceLocationAnswer {
    location: Point;
}

interface ServiceLocationsAnswer {
    locations: {latitude:number, longitude:number, timestamp:number}[];
}

@Injectable()
export class PlayerService {
  public storageKeyPrefix:string = 'player-service--';
  private intervalId:number = null;
  public training:Training = null;
  public trainings:Training[] = [];
  public positionUpdatedEvent: EventEmitter<Point> = new EventEmitter();
  public routeUpdatedEvent: EventEmitter<any> = new EventEmitter();

  constructor() {
    this.restoreState();
    this.loadTrainings();
    this.intervalId = setInterval(() => {
      this.updatePosition();
    }, 5000);
  };

  public startTraining() {
    if (this.training == null) {
      InjectionPlugin.startSelection(() => {
        this.training = new Training();
        this.training.start();
      });
    }
  }

  public stopTraining() {
    if (this.training.status == TrainingStatus.Working) {
      InjectionPlugin.stopSelection(() => {
        this.training.stop();
        this.trainings.push(this.training);
        this.training = null;
        this.saveTrainings();
      });
    }
  }

  public saveState() {
    let key = this.storageKeyPrefix + '-training';
    window.localStorage[key] = JSON.stringify(this.training);
  }

  public restoreState() {
    let key = this.storageKeyPrefix + '-training';
    if (!(key in window.localStorage)) {
      return;
    }
    let dict:any = JSON.parse(window.localStorage[key])
    this.training = Training.fromDict(dict);
  }

  public loadTrainings() {
    let key = this.storageKeyPrefix + '-trainings';
    if (!(key in window.localStorage)) {
      return;
    }
    let list:any = JSON.parse(window.localStorage[key]);
    this.trainings = [];
    for(let dict of list) {
      this.trainings.push(Training.fromDict(dict));
    }
  }

  public saveTrainings() {
    let key = this.storageKeyPrefix + '-trainings';
    window.localStorage[key] = JSON.stringify(this.trainings);
  }

  public updatePosition() {
    InjectionPlugin.getLocation((data) => {
      if ('location' in data) {
        var point:Point = Point.fromDict({
          lat: data.location.latitude,
          lng: data.location.longitude,
          timestamp: data.location.timestamp
        });
        this.positionUpdatedEvent.emit(point);
      }
    });
    if (this.training != null && this.training.status == TrainingStatus.Working) {
      InjectionPlugin.getLocations((data:ServiceLocationsAnswer) => {
        if ('locations' in data) {
          for (let location of data.locations) {
            let point:Point = Point.fromDict({
              lat: location.latitude,
              lng: location.longitude,
              timestamp: location.timestamp
            });
            this.training.points.push(point);
          }
          this.routeUpdatedEvent.emit({});
        }
      });
    };
  }
}

export class Video {

  constructor(
    public id:number,
    public title:string,
    public videoCode:string,
    public desc:string
  ) {

  }
}

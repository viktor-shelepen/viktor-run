///<reference path="../node_modules/angular2/typings/browser.d.ts"/>

import {provide} from 'angular2/core';
import {bootstrap} from 'angular2/platform/browser';
import {AppComponent} from './component/app';
import {ROUTER_PROVIDERS} from 'angular2/router';
import {LocationStrategy, HashLocationStrategy, APP_BASE_HREF} from 'angular2/router';
import {PlayerService} from './service/player';

bootstrap(AppComponent, [
    PlayerService,
    ROUTER_PROVIDERS,
    provide(APP_BASE_HREF, {useValue : '/' }),
    provide(LocationStrategy,{useClass: HashLocationStrategy})
]);

///<reference path="../../typings/googlemaps/google.maps.d.ts"/>
System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var Point;
    return {
        setters:[],
        execute: function() {
            Point = (function () {
                function Point(latLng, timestamp) {
                    this.latLng = latLng;
                    this.timestamp = timestamp;
                }
                Point.fromDict = function (dict) {
                    var point = new Point(new google.maps.LatLng(dict.lat, dict.lng), dict.timestamp);
                    return point;
                };
                Point.prototype.toJSON = function () {
                    return {
                        latLng: this.latLng,
                        timestamp: this.timestamp
                    };
                };
                return Point;
            }());
            exports_1("Point", Point);
        }
    }
});

System.register(['./point'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var point_1;
    var TrainingStatus, Training;
    return {
        setters:[
            function (point_1_1) {
                point_1 = point_1_1;
            }],
        execute: function() {
            (function (TrainingStatus) {
                TrainingStatus[TrainingStatus["NotDefied"] = 0] = "NotDefied";
                TrainingStatus[TrainingStatus["Working"] = 1] = "Working";
                TrainingStatus[TrainingStatus["Stoped"] = 2] = "Stoped";
            })(TrainingStatus || (TrainingStatus = {}));
            exports_1("TrainingStatus", TrainingStatus);
            Training = (function () {
                function Training() {
                    this.startTime = null;
                    this.finishTime = null;
                    this.status = TrainingStatus.NotDefied;
                    this.points = [];
                }
                Training.fromDict = function (dict) {
                    var training = new Training();
                    var startTime = dict.startTicks == 'null' ? null : new Date(dict.startTicks);
                    var finishTime = dict.finishTicks == 'null' ? null : new Date(dict.finishTicks);
                    training.startTime = startTime;
                    training.finishTime = finishTime;
                    training.status = dict.status;
                    for (var _i = 0, _a = dict.points; _i < _a.length; _i++) {
                        var pointDict = _a[_i];
                        training.points.push(point_1.Point.fromDict(pointDict));
                    }
                    return training;
                };
                Training.prototype.toJSON = function () {
                    var startTicks = this.startTime != null ? this.startTime.valueOf() : 'null';
                    var finishTicks = this.finishTime != null ? this.finishTime.valueOf() : 'null';
                    return {
                        startTicks: startTicks,
                        finishTicks: finishTicks,
                        status: this.status,
                        points: this.points
                    };
                };
                Training.prototype.start = function () {
                    if (this.status == TrainingStatus.Stoped) {
                        throw new Error('The training could not be started.');
                    }
                    this.startTime = new Date();
                    this.status = TrainingStatus.Working;
                };
                Training.prototype.stop = function () {
                    if (this.status != TrainingStatus.Working) {
                        throw new Error('The not working training could not be stoped.');
                    }
                    this.finishTime = new Date();
                    this.status = TrainingStatus.Stoped;
                };
                Training.prototype.getDuration = function () {
                    var endTime = this.finishTime || new Date();
                    var diff = endTime.getTime() - this.startTime.getTime();
                    return diff;
                };
                return Training;
            }());
            exports_1("Training", Training);
        }
    }
});

System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var ConfigService;
    return {
        setters:[],
        execute: function() {
            ConfigService = (function () {
                function ConfigService() {
                }
                ConfigService.HEADING_TITLE = "Hello from Viktor!";
                ConfigService.MODE = runningMode || 'mobile';
                return ConfigService;
            }());
            exports_1("ConfigService", ConfigService);
        }
    }
});

System.register(['angular2/core', './page/map', 'angular2/router', './page/player', './page/history'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, map_1, router_1, player_1, history_1;
    var AppComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (map_1_1) {
                map_1 = map_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (player_1_1) {
                player_1 = player_1_1;
            },
            function (history_1_1) {
                history_1 = history_1_1;
            }],
        execute: function() {
            AppComponent = (function () {
                function AppComponent() {
                }
                AppComponent.prototype.reloadPage = function () {
                    window.location.reload();
                };
                AppComponent.prototype.clearCache = function () {
                    window.localStorage.clear();
                    window.location.reload();
                };
                AppComponent = __decorate([
                    core_1.Component({
                        selector: 'my-app',
                        templateUrl: './templates/component/app.html',
                        directives: [router_1.ROUTER_DIRECTIVES],
                        providers: []
                    }),
                    router_1.RouteConfig([
                        {
                            path: '/player',
                            name: 'PlayerPage',
                            component: player_1.PlayerPageComponent,
                            useAsDefault: true
                        },
                        { path: '/map', name: 'MapPage', component: map_1.MapPageComponent },
                        { path: '/history', name: 'HistoryPage', component: history_1.HistoryPageComponent },
                    ]), 
                    __metadata('design:paramtypes', [])
                ], AppComponent);
                return AppComponent;
            }());
            exports_1("AppComponent", AppComponent);
        }
    }
});

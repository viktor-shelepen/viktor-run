System.register(['angular2/core', '../../service/player', '../../api/trainig'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, player_1, trainig_1;
    var PlayerPageComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (player_1_1) {
                player_1 = player_1_1;
            },
            function (trainig_1_1) {
                trainig_1 = trainig_1_1;
            }],
        execute: function() {
            PlayerPageComponent = (function () {
                function PlayerPageComponent(playerService) {
                    this.playerService = playerService;
                    this.toggleLabel = 'Start';
                    if (this.playerService.training != null
                        && this.playerService.training.status == trainig_1.TrainingStatus.Working) {
                        this.toggleLabel = 'Stop';
                    }
                }
                PlayerPageComponent.prototype.toggleTraining = function () {
                    if (this.playerService.training == null) {
                        this.playerService.startTraining();
                        this.toggleLabel = 'Stop';
                    }
                    else {
                        this.playerService.stopTraining();
                        this.toggleLabel = 'Start';
                    }
                };
                PlayerPageComponent = __decorate([
                    core_1.Component({
                        selector: 'player-page',
                        templateUrl: './templates/component/page/player.html',
                        providers: []
                    }), 
                    __metadata('design:paramtypes', [player_1.PlayerService])
                ], PlayerPageComponent);
                return PlayerPageComponent;
            }());
            exports_1("PlayerPageComponent", PlayerPageComponent);
        }
    }
});

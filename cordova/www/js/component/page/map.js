///<reference path="../../../typings/googlemaps/google.maps.d.ts"/>
System.register(['angular2/core', '../../service/player'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, player_1;
    var MapPageComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (player_1_1) {
                player_1 = player_1_1;
            }],
        execute: function() {
            MapPageComponent = (function () {
                function MapPageComponent(_elem, playerService) {
                    var _this = this;
                    this._elem = _elem;
                    this.playerService = playerService;
                    this.map = null;
                    this.marker = null;
                    this.route = null;
                    this.currentPoint = null;
                    this.playerService.positionUpdatedEvent.subscribe(function (point) {
                        _this.currentPoint = point;
                        _this.map.setCenter(point.latLng);
                        _this.marker.setPosition(point.latLng);
                    });
                    this.playerService.routeUpdatedEvent.subscribe(function (data) {
                        var path = _this.getPath();
                        _this.route.setPath(path);
                    });
                }
                MapPageComponent.prototype.getPath = function () {
                    if (this.playerService.training == null) {
                        return [];
                    }
                    var path = [];
                    for (var _i = 0, _a = this.playerService.training.points; _i < _a.length; _i++) {
                        var point = _a[_i];
                        path.push(point.latLng);
                    }
                    return path;
                };
                MapPageComponent.prototype.ngOnInit = function () {
                    var container = this._elem.nativeElement.querySelector('.map');
                    this.map = new google.maps.Map(container, {
                        center: { lat: -34.397, lng: 150.644 },
                        zoom: 13
                    });
                    this.route = new google.maps.Polyline({
                        geodesic: true,
                        strokeColor: '#FF0000',
                        strokeOpacity: 1.0,
                        strokeWeight: 2
                    });
                    this.route.setMap(this.map);
                    this.marker = new google.maps.Marker({
                        icon: {
                            path: google.maps.SymbolPath.BACKWARD_CLOSED_ARROW,
                            scale: 2
                        },
                        draggable: true,
                        map: this.map
                    });
                };
                MapPageComponent.prototype.ngOnDestroy = function () {
                };
                MapPageComponent = __decorate([
                    core_1.Component({
                        selector: 'map-page',
                        templateUrl: './templates/component/page/map.html',
                        styles: ["\n      .map {\n        height: 80vh;\n        width: 100%;\n      }\n    "]
                    }), 
                    __metadata('design:paramtypes', [core_1.ElementRef, player_1.PlayerService])
                ], MapPageComponent);
                return MapPageComponent;
            }());
            exports_1("MapPageComponent", MapPageComponent);
        }
    }
});
//# sourceMappingURL=map.js.map
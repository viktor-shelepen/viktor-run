/// <reference path="./typings/systemjs/systemjs.d.ts"/>
function runApp() {
    System.config({
        baseUrl: baseUrl,
        packages: {
            'js': {
                format: 'register',
                defaultExtension: 'js'
            }
        }
    });
    System.import(baseUrl + 'js/main.js')
        .then(null, console.error.bind(console));
}
;
function deviceReady() {
    var element = document.getElementById('startScreen');
    var parentEl = element.parentNode;
    parentEl.removeChild(element);
    runApp();
}
;
document.addEventListener("deviceready", function () {
    deviceReady();
}, false);

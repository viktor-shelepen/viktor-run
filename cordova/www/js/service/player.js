System.register(['angular2/core', '../api/trainig', '../api/point'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, trainig_1, point_1;
    var PlayerService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (trainig_1_1) {
                trainig_1 = trainig_1_1;
            },
            function (point_1_1) {
                point_1 = point_1_1;
            }],
        execute: function() {
            PlayerService = (function () {
                function PlayerService() {
                    var _this = this;
                    this.storageKeyPrefix = 'player-service--';
                    this.intervalId = null;
                    this.training = null;
                    this.trainings = [];
                    this.positionUpdatedEvent = new core_1.EventEmitter();
                    this.routeUpdatedEvent = new core_1.EventEmitter();
                    this.restoreState();
                    this.loadTrainings();
                    this.intervalId = setInterval(function () {
                        _this.updatePosition();
                    }, 5000);
                }
                ;
                PlayerService.prototype.startTraining = function () {
                    var _this = this;
                    if (this.training == null) {
                        InjectionPlugin.startSelection(function () {
                            _this.training = new trainig_1.Training();
                            _this.training.start();
                        });
                    }
                };
                PlayerService.prototype.stopTraining = function () {
                    var _this = this;
                    if (this.training.status == trainig_1.TrainingStatus.Working) {
                        InjectionPlugin.stopSelection(function () {
                            _this.training.stop();
                            _this.trainings.push(_this.training);
                            _this.training = null;
                            _this.saveTrainings();
                        });
                    }
                };
                PlayerService.prototype.saveState = function () {
                    var key = this.storageKeyPrefix + '-training';
                    window.localStorage[key] = JSON.stringify(this.training);
                };
                PlayerService.prototype.restoreState = function () {
                    var key = this.storageKeyPrefix + '-training';
                    if (!(key in window.localStorage)) {
                        return;
                    }
                    var dict = JSON.parse(window.localStorage[key]);
                    this.training = trainig_1.Training.fromDict(dict);
                };
                PlayerService.prototype.loadTrainings = function () {
                    var key = this.storageKeyPrefix + '-trainings';
                    if (!(key in window.localStorage)) {
                        return;
                    }
                    var list = JSON.parse(window.localStorage[key]);
                    this.trainings = [];
                    for (var _i = 0, list_1 = list; _i < list_1.length; _i++) {
                        var dict = list_1[_i];
                        this.trainings.push(trainig_1.Training.fromDict(dict));
                    }
                };
                PlayerService.prototype.saveTrainings = function () {
                    var key = this.storageKeyPrefix + '-trainings';
                    window.localStorage[key] = JSON.stringify(this.trainings);
                };
                PlayerService.prototype.updatePosition = function () {
                    var _this = this;
                    InjectionPlugin.getLocation(function (data) {
                        if ('location' in data) {
                            var point = point_1.Point.fromDict({
                                lat: data.location.latitude,
                                lng: data.location.longitude,
                                timestamp: data.location.timestamp
                            });
                            _this.positionUpdatedEvent.emit(point);
                        }
                    });
                    if (this.training != null && this.training.status == trainig_1.TrainingStatus.Working) {
                        InjectionPlugin.getLocations(function (data) {
                            if ('locations' in data) {
                                for (var _i = 0, _a = data.locations; _i < _a.length; _i++) {
                                    var location_1 = _a[_i];
                                    var point = point_1.Point.fromDict({
                                        lat: location_1.latitude,
                                        lng: location_1.longitude,
                                        timestamp: location_1.timestamp
                                    });
                                    _this.training.points.push(point);
                                }
                                _this.routeUpdatedEvent.emit({});
                            }
                        });
                    }
                    ;
                };
                PlayerService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [])
                ], PlayerService);
                return PlayerService;
            }());
            exports_1("PlayerService", PlayerService);
        }
    }
});
//# sourceMappingURL=player.js.map
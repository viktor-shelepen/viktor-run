///<reference path="../node_modules/angular2/typings/browser.d.ts"/>
System.register(['angular2/core', 'angular2/platform/browser', './component/app', 'angular2/router', './service/player'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var core_1, browser_1, app_1, router_1, router_2, player_1;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (browser_1_1) {
                browser_1 = browser_1_1;
            },
            function (app_1_1) {
                app_1 = app_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
                router_2 = router_1_1;
            },
            function (player_1_1) {
                player_1 = player_1_1;
            }],
        execute: function() {
            browser_1.bootstrap(app_1.AppComponent, [
                player_1.PlayerService,
                router_1.ROUTER_PROVIDERS,
                core_1.provide(router_2.APP_BASE_HREF, { useValue: '/' }),
                core_1.provide(router_2.LocationStrategy, { useClass: router_2.HashLocationStrategy })
            ]);
        }
    }
});

module.exports = {
    startSelection: function (successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "InjectionPlugin", "startSelection", [])
    },
    stopSelection: function (successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "InjectionPlugin", "stopSelection", [])
    },
    getLocation: function (successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "InjectionPlugin", "getLocation", [])
    },
    getLocations: function (successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "InjectionPlugin", "getLocations", [])
    }
}

package ua.shelepen.viktor_run.plugin;

import org.apache.cordova.*;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.ComponentName;
import android.os.IBinder;
import android.location.Location;
import android.util.Log;


public class InjectionPlugin extends CordovaPlugin {
    private static final String TAG = "BOOMBOOMTESTGPS";
    private Intent serviceIntent = null;
    private BackgroundService backgroundService;

    /**
     * Defines callbacks for serviceIntent binding, passed to bindService()
     */
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            BackgroundService.LocalBinder binder = (BackgroundService.LocalBinder) service;
            backgroundService = binder.getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            backgroundService = null;
        }
    };


    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        Context context = cordova.getActivity().getApplicationContext();
        this.serviceIntent = new Intent(context, BackgroundService.class);
        context.bindService(this.serviceIntent, mConnection, Context.BIND_AUTO_CREATE);
        context.startService(this.serviceIntent);
    }

    public JSONObject getJSONLocation(Location location) throws JSONException {
        JSONObject JSONLocation = new JSONObject();
        JSONLocation.put("altitude", location.getAltitude());
        JSONLocation.put("latitude", location.getLatitude());
        JSONLocation.put("longitude", location.getLongitude());
        JSONLocation.put("accuracy", location.getAccuracy());
        JSONLocation.put("speed", location.getSpeed());
        JSONLocation.put("timestamp", location.getTime());

        return JSONLocation;
    }

    @Override
    public boolean execute(String action, JSONArray data, final CallbackContext callbackContext) throws JSONException {
        if (action.equals("startSelection")) {
            backgroundService.selectLocations = true;
            return true;
        } else if (action.equals("stopSelection")) {
            backgroundService.selectLocations = false;
            return true;
        } else if (action.equals("getLocation")) {
            Location location = (Location) backgroundService.lastLocation;
            JSONObject parameter = new JSONObject();
            JSONObject JSONLocation = this.getJSONLocation(location);
            parameter.put("location", JSONLocation);
            callbackContext.success(parameter);
            return true;
        } else if (action.equals("getLocations")) {
            JSONObject parameter = new JSONObject();
            parameter.put("state", "eventState");
            parameter.put("index", "Index");
            JSONArray locations = new JSONArray();
            if (backgroundService == null) {
                callbackContext.error("The background service is not ready.");
                return false;
            }
            while (!backgroundService.locations.empty()) {
                Location location = (Location) backgroundService.locations.pop();
                JSONObject JSONLocation = this.getJSONLocation(location);
                locations.put(JSONLocation);
            }

            Log.e(TAG, "onGetLocations received ...");
            parameter.put("locations", locations);
            callbackContext.success(parameter);
            return true;
        } else {
            return false;
        }
    }
}

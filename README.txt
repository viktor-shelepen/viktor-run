


Development components:
- atom
- atom-typescript
- nodejs
- node-inspector
- gulp
- typescript
- tsd
- express
- body-parser



Articles:
- Cordova Browser-Sync Plugin
https://www.npmjs.com/package/cordova-plugin-browsersync

- Atom + TypeScript.
https://github.com/node-inspector/node-inspector

- REST API using Node.js.
http://blog.geraldpereira.com/rest/crud/2015/09/10/nodejs-express-typescript.html
